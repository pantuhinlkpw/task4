program be2;

var
  x: char;
  b: boolean;

begin
  writeln('Pantyukhin Lev, Group 112, exercise 6.23c');
  write('Input text: ');
  b := true;
  read(x);
  if (b or (x <> 'b')) and (x<>'.') then write(x);
  if x = 'c' then b := false
  else b := true;
  while x <> '.' do begin
  read(x);
    if (b or (x <> 'b')) and (x<>'.') then write(x);
    if x = 'c' then b := false
    else b := true;
  end;
  writeln();
end.