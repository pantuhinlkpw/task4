#include <iostream>
#include <vector>
#include <iterator>
#include "Scanner.h"
#include "Parser.h"
#include "Poliz.h"
using namespace std;

int main(int argc, char **argv) {
	try {
		Poliz kek(argv[1]);
	}
	catch (Lex & L) {
		cerr << endl << "-----------------" << endl;
		cerr << "Wrong lexem: " << L.get_name() << endl;
		cerr << "Line: "<< L.get_line() << endl;
		cerr << "Column: " << L.get_column() << endl;
		cerr << "Syntax error" << endl;
	}
	catch (const string & s) {
		cerr << endl << "-----------------" << endl;
		cerr << s << endl;
		cerr << "Error of execution" << endl;
	}
	catch (const char *s) {
		cerr << endl << "-----------------" << endl;
		cerr << s << endl;
		cerr << "Error of execution" << endl;
	}
	catch (Var & v) {
		cerr << endl << "-----------------" << endl;
		cerr << v.get_name() << " is constant" << endl;
		cerr << "Error of execution" << endl;
	}
	catch (char c) {
		cerr << endl << "-----------------" << endl;
		cerr << "Invalid symbol: " << c << endl;
		cerr << "Lexical error" << endl;
	}
	catch (...) {
		return 1;
	};
	return 0;
}