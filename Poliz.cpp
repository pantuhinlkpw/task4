#include <iostream>
#include <vector>
#include <iterator>
#include <stack>
#include "Scanner.h"
#include "Parser.h"
#include "Poliz.h"
using namespace std;

bool Poliz::check_operator(string str) {
	static vector<string> Operators = {":=", "+", "-", "/", "*", 
	"div", "mod", "or", "and", "not", "=", "<>", "<", "<=", ">", ">="};
	if (Scanner::search(Operators, str)) return true;
	else return false;
}

void Poliz::adjust_operand_lex(Lex & lex) {
	bool var = false;
	switch (lex.get_type()) {
		case LEX_NUM:
			if (Scanner::check_NUM(lex.get_name())) 
				lex.set_type(POL_INT);
			else 
				lex.set_type(POL_REAL);
			break;
		case LEX_NAME: 
			if ((lex.get_name() == "false") || (lex.get_name() == "true"))
				lex.set_type(POL_BOOL);
			else {
				var = true;
				switch (list_v[search_var(lex.get_name())].get_type()) {
					case VAR_INT: lex.set_type(POL_INT); break;
					case VAR_REAL: lex.set_type(POL_REAL); break;
					case VAR_BOOL: lex.set_type(POL_BOOL); break;
					case VAR_CHAR: lex.set_type(POL_CHAR); break;
					default: throw "Error in adjust_operand_lex()"; break;
				}
			}
			break;
		case LEX_STR:
			if (lex.get_name().size() == 3)
				lex.set_type(POL_CHAR);
			else
				lex.set_type(POL_STR);
			{ 
			string tmp(lex.get_name());
			tmp.erase(0, 1);
			tmp.erase(tmp.size() - 1, 1);
			lex.set_name(tmp);
			}
			break;
		default: ;
			throw "Error in adjust_operand_lex()";
			break;
	}
	lex.set_var(var);
}

void Poliz::adjust_operator_lex(Lex & lex) {
	static vector<string> Operators0 = {"not"};
	static vector<string> Operators1 = {"/", "*", "div", "mod", "and"};
	static vector<string> Operators2 = {"+", "-", "or"};
	static vector<string> Operators3 = {"=", "<>", "<", "<=", ">", ">="};
	static vector<string> Operators4 = {":="};
	lex.set_type(POL_OP);
	if (Scanner::search(Operators1, lex.get_name())) lex.set_priority(1);
	else if (Scanner::search(Operators2, lex.get_name())) lex.set_priority(2);
	else if (Scanner::search(Operators3, lex.get_name())) lex.set_priority(3);
	else if (Scanner::search(Operators4, lex.get_name())) lex.set_priority(4);
}

vector<Lex>::iterator Poliz::getlex() {
	if (++cur_lex != list_l.end())
		return cur_lex;
	return list_l.end();
}

void Poliz::pol_block() {
	if (cur_lex->get_name() != "begin") return;
	cur_lex = getlex();
	pol_s();
	while (cur_lex->get_name() == ";") {
		cur_lex->set_type(POL_SPEC);
		poliz.insert(poliz.end(), *cur_lex);
		cur_lex = getlex();
		if (cur_lex->get_name() != "end") pol_s();
		else break;
	}
	if (cur_lex->get_name() == "end") cur_lex = getlex();
	else throw "Error in pol_block()";

}

void Poliz::pol_s() {
	if (cur_lex->get_name() == "if") {
		int pol_jump;
		pol_expr();
		pol_jump = poliz.size();
		poliz.insert(poliz.end(), Lex::false_jump());
		cur_lex = getlex();
		if (cur_lex->get_name() == "begin") pol_block();
		else pol_s();
		if (cur_lex->get_name() == "else") {
			poliz.insert(poliz.end(), Lex::uncon_jump());
			poliz[pol_jump].set_jump(poliz.size());
			pol_jump = poliz.size() - 1;
			cur_lex = getlex();
			if (cur_lex->get_name() == "begin") pol_block();
			else pol_s();
		} 
		poliz[pol_jump].set_jump(poliz.size() + 1);

	} else if (cur_lex->get_name() == "while") {
		int pol_jump;
		int pol_jump_uncon = poliz.size();
		pol_expr();
		cur_lex = getlex();
		poliz.insert(poliz.end(), Lex::false_jump());
		pol_jump = poliz.size() - 1;
		if (cur_lex->get_name() == "begin") pol_block();
		else pol_s();
		poliz.insert(poliz.end(), Lex::uncon_jump());
		poliz[poliz.size() - 1].set_jump(pol_jump_uncon);
		poliz[pol_jump].set_jump(poliz.size());

	} else if (cur_lex->get_name() == "for") {
		cur_lex = getlex();
		Lex tmp = *cur_lex;
		adjust_operand_lex(tmp);
		pol_expr();
		poliz.insert(poliz.end(), Lex(";", POL_SPEC, 0 ,0));
		bool flag = true;
		if (cur_lex->get_name() == "downto") flag = false;
		int pol_jump;
		int pol_jump_uncon = poliz.size();
		poliz.insert(poliz.end(), tmp);
		cur_lex = getlex();
		pol_expr();
		if (flag) poliz.insert(poliz.end(), Lex("<=", POL_OP, 0 ,0, 3));
		else poliz.insert(poliz.end(), Lex(">=", POL_OP, 0 ,0, 3));
		cur_lex = getlex();
		poliz.insert(poliz.end(), Lex::false_jump());
		pol_jump = poliz.size() - 1;
		if (cur_lex->get_name() == "begin") pol_block();
		else pol_s();
		poliz.insert(poliz.end(), tmp);
		poliz.insert(poliz.end(), tmp);
		poliz.insert(poliz.end(), Lex("1", POL_INT, 0 ,0));
		if (flag) poliz.insert(poliz.end(), Lex("+", POL_OP, 0 ,0, 2));
		else poliz.insert(poliz.end(), Lex("-", POL_OP, 0 ,0, 2));
		poliz.insert(poliz.end(), Lex(":=", POL_OP, 0 ,0, 4));
		poliz.insert(poliz.end(), Lex::uncon_jump());
		poliz[poliz.size() - 1].set_jump(pol_jump_uncon);
		poliz[pol_jump].set_jump(poliz.size());
	
	} else if ((cur_lex->get_name() == "read") || (cur_lex->get_name() == "readln") || 
		(cur_lex->get_name() == "write") || (cur_lex->get_name() == "writeln")) {
		cur_lex->set_type(POL_IO);
		Lex tmp = *cur_lex;
		if ((cur_lex->get_name() == "read") || (cur_lex->get_name() == "readln")) poliz.insert(poliz.end(), Lex("<<", POL_IO));
		else if ((cur_lex->get_name() == "write") || (cur_lex->get_name() == "writeln")) poliz.insert(poliz.end(), Lex(">>", POL_IO));
		cur_lex = getlex();
		pol_expr();
		poliz.insert(poliz.end(), tmp);

	} else {
		pol_expr();
	}
	if (!queue.empty()) throw "Stack isn't empty";
	return;
}

void Poliz::pol_expr() {
	for (; true; cur_lex = getlex()) {

		// opreand
		if ((cur_lex->get_type() == LEX_NUM) || (cur_lex->get_type() == LEX_NAME) || (cur_lex->get_type() == LEX_STR)) {
			adjust_operand_lex(*cur_lex);
			poliz.insert(poliz.end(), *cur_lex);

		// opeator
		} else if (check_operator(cur_lex->get_name())) {
			adjust_operator_lex(*cur_lex);
			while ((!queue.empty()) && (queue.top().get_name() != "(") && (queue.top().get_priority() <= cur_lex->get_priority())) {
					poliz.insert(poliz.end(), queue.top());
					queue.pop();
			}
			queue.push(*cur_lex);

		} else if ((cur_lex->get_name() == "then") || (cur_lex->get_name() == "do") || (cur_lex->get_name() == "else") || 
			(cur_lex->get_name() == "to") || (cur_lex->get_name() == "downto") || (cur_lex->get_name() == ";") || 
			(cur_lex->get_name() == "end")) {
			// end of expression
			while (!queue.empty()) {
				poliz.insert(poliz.end(), queue.top());
				queue.pop();
			}
			return;

		} else if (cur_lex->get_name() == "(") {
			queue.push(*cur_lex);

		} else if (cur_lex->get_name() == ")") {
			if (!queue.empty()) {
				while (queue.top().get_name() != "(") {
					poliz.insert(poliz.end(), queue.top());
					queue.pop();
				}
				queue.pop();
			} else throw "Error in pol_expr()";	

		} else if (cur_lex->get_name() == ",") {
			while ((!queue.empty()) && (queue.top().get_name() != "(")) {
				poliz.insert(poliz.end(), queue.top());
				queue.pop();	
			}
		}
	}
}

int Poliz::search_var(string name) const {
	for (auto T = list_v.begin(); T != list_v.end(); ++T) {
		if (T->get_name() == name) return distance(list_v.begin(), T);
	}
	throw name + " isn't declared";
}

void Poliz::execute() {
	if (!queue.empty()) throw "Stack isn't empty";
	Lex curlex;
	for (unsigned int i = 0; i <= poliz.size(); ++i) {
		curlex = poliz[i];

		// operands
		if ((curlex.get_type() == POL_INT) || (curlex.get_type() == POL_REAL) || 
		(curlex.get_type() == POL_BOOL) || (curlex.get_type() == POL_CHAR) || (curlex.get_type() == POL_STR)) {
			queue.push(curlex);

		//operators
		} else if (curlex.get_type() == POL_OP) {
			Lex a, b;
			b = queue.top();
			queue.pop();
			if (curlex.get_name() != "not") {
				a = queue.top();
				queue.pop();
			}
			// operator *
			if (curlex.get_name() == "*") {
				if (((a.get_type() == POL_INT) || (a.get_type() == POL_REAL)) && ((b.get_type() == POL_INT) || (b.get_type() == POL_REAL))) {
					double x, y;
					if (a.get_type() == POL_INT) x = Lex_to_int(a); 
					else x = Lex_to_double(a);
					if (b.get_type() == POL_INT) y = Lex_to_int(b); 
					else y = Lex_to_double(b);
					x = x * y;
					if ((a.get_type() == POL_INT) && (b.get_type() == POL_INT)) queue.push(Lex(to_string((int)x), POL_INT));
					else queue.push(Lex(to_string(x), POL_REAL));
				} else throw "Invalid types of variables";

			// operator /
			} else if (curlex.get_name() == "/") {
				if (((a.get_type() == POL_INT) || (a.get_type() == POL_REAL)) && ((b.get_type() == POL_INT) || (b.get_type() == POL_REAL))) {
					double x, y;
					if (a.get_type() == POL_INT) x = Lex_to_int(a); 
					else x = Lex_to_double(a);
					if (b.get_type() == POL_INT) y = Lex_to_int(b); 
					else y = Lex_to_double(b);
					x = x / y;
					if ((a.get_type() == POL_INT) && (b.get_type() == POL_INT)) queue.push(Lex(to_string((int)x), POL_INT)); 
					else queue.push(Lex(to_string(x), POL_REAL));
				} else throw "Invalid types of variables";

			// operator div
			} else if (curlex.get_name() == "div") {
				if ((a.get_type() == POL_INT) && (b.get_type() == POL_INT))
					queue.push(Lex(to_string(Lex_to_int(a) / Lex_to_int(b)), POL_INT));
				else throw "Different types of operands";

			// operator mod
			} else if (curlex.get_name() == "mod") {
				if ((a.get_type() == POL_INT) && (b.get_type() == POL_INT))
					queue.push(Lex(to_string(Lex_to_int(a) % Lex_to_int(b)), POL_INT));
				else throw "Different types of operands";

			// operator and
			} else if (curlex.get_name() == "and") {
				if ((a.get_type() == POL_BOOL) && (b.get_type() == POL_BOOL))
					queue.push(Lex(to_string(Lex_to_bool(a) && Lex_to_bool(b)), POL_BOOL));
				else throw "Different types of operands";

			// operator +
			} else if (curlex.get_name() == "+") {
				if (((a.get_type() == POL_INT) || (a.get_type() == POL_REAL)) && ((b.get_type() == POL_INT) || (b.get_type() == POL_REAL))) {
					double x, y;
					if (a.get_type() == POL_INT) x = Lex_to_int(a); 
					else x = Lex_to_double(a);
					if (b.get_type() == POL_INT) y = Lex_to_int(b); 
					else y = Lex_to_double(b);
					x = x + y;
					if ((a.get_type() == POL_INT) && (b.get_type() == POL_INT)) queue.push(Lex(to_string((int)x), POL_INT));
					else queue.push(Lex(to_string(x), POL_REAL));
				} else throw "Invalid types of variables";

			// operator -
			} else if (curlex.get_name() == "-") {
				if (((a.get_type() == POL_INT) || (a.get_type() == POL_REAL)) && ((b.get_type() == POL_INT) || (b.get_type() == POL_REAL))) {
					double x, y;
					if (a.get_type() == POL_INT) x = Lex_to_int(a); 
					else x = Lex_to_double(a);
					if (b.get_type() == POL_INT) y = Lex_to_int(b); 
					else y = Lex_to_double(b);
					x = x - y;
					if ((a.get_type() == POL_INT) && (b.get_type() == POL_INT)) queue.push(Lex(to_string((int)x), POL_INT));
					else queue.push(Lex(to_string(x), POL_REAL));
				} else throw "Invalid types of variables";

			// operator or
			} else if (curlex.get_name() == "or") {
				if ((a.get_type() == POL_BOOL) && (b.get_type() == POL_BOOL))
					queue.push(Lex(to_string(Lex_to_bool(a) || Lex_to_bool(b)), POL_BOOL));
				else throw "Different types of operands";

			// operator =
			} else if (curlex.get_name() == "=") {
				if (a.get_type() == b.get_type()) {
					bool res;
					if (a.get_type() == POL_INT) res = Lex_to_int(a) == Lex_to_int(b);
					else if (a.get_type() == POL_REAL) res = Lex_to_double(a) == Lex_to_double(b);
					else if (a.get_type() == POL_BOOL) res = Lex_to_bool(a) == Lex_to_bool(b);
					else if (a.get_type() == POL_CHAR) res = Lex_to_char(a) == Lex_to_char(b);
					else throw "Wrong type of operand";
					queue.push(Lex(to_string(res), POL_BOOL));
				} else throw "Different types of operands";

			// operator <>
			} else if (curlex.get_name() == "<>") {
				if (a.get_type() == b.get_type()) {
					bool res;
					if (a.get_type() == POL_INT) res = Lex_to_int(a) != Lex_to_int(b);
					else if (a.get_type() == POL_REAL) res = Lex_to_double(a) != Lex_to_double(b);
					else if (a.get_type() == POL_BOOL) res = Lex_to_bool(a) != Lex_to_bool(b);
					else if (a.get_type() == POL_CHAR) res = Lex_to_char(a) != Lex_to_char(b);
					else throw "Wrong type of operand";
					queue.push(Lex(to_string(res), POL_BOOL));
				} else throw "Different types of operands";

			// operator <
			} else if (curlex.get_name() == "<") {
				if (a.get_type() == b.get_type()) {
					bool res;
					if (a.get_type() == POL_INT) res = Lex_to_int(a) < Lex_to_int(b);
					else if (a.get_type() == POL_REAL) res = Lex_to_double(a) < Lex_to_double(b);
					else if (a.get_type() == POL_BOOL) res = Lex_to_bool(a) < Lex_to_bool(b);
					else if (a.get_type() == POL_CHAR) res = Lex_to_char(a) < Lex_to_char(b);
					else throw "Wrong type of operand";
					queue.push(Lex(to_string(res), POL_BOOL));
				} else throw "Different types of operands";

			// operator <=
			} else if (curlex.get_name() == "<=") {
				if (a.get_type() == b.get_type()) {
					bool res;
					if (a.get_type() == POL_INT) res = Lex_to_int(a) <= Lex_to_int(b);
					else if (a.get_type() == POL_REAL) res = Lex_to_double(a) <= Lex_to_double(b);
					else if (a.get_type() == POL_BOOL) res = Lex_to_bool(a) <= Lex_to_bool(b);
					else if (a.get_type() == POL_CHAR) res = Lex_to_char(a) <= Lex_to_char(b);
					else throw "Wrong type of operand";
					queue.push(Lex(to_string(res), POL_BOOL));
				} else throw "Different types of operands";

			// operator >
			} else if (curlex.get_name() == ">") {
				if (a.get_type() == b.get_type()) {
					bool res;
					if (a.get_type() == POL_INT) res = Lex_to_int(a) > Lex_to_int(b);
					else if (a.get_type() == POL_REAL) res = Lex_to_double(a) > Lex_to_double(b);
					else if (a.get_type() == POL_BOOL) res = Lex_to_bool(a) > Lex_to_bool(b);
					else if (a.get_type() == POL_CHAR) res = Lex_to_char(a) > Lex_to_char(b);
					else throw "Wrong type of operand";
					queue.push(Lex(to_string(res), POL_BOOL));
				} else throw "Different types of operands";

			// operator >=
			} else if (curlex.get_name() == ">=") {
				if (a.get_type() == b.get_type()) {
					bool res;
					if (a.get_type() == POL_INT) res = Lex_to_int(a) >= Lex_to_int(b);
					else if (a.get_type() == POL_REAL) res = Lex_to_double(a) >= Lex_to_double(b);
					else if (a.get_type() == POL_BOOL) res = Lex_to_bool(a) >= Lex_to_bool(b);
					else if (a.get_type() == POL_CHAR) res = Lex_to_char(a) >= Lex_to_char(b);
					else throw "Wrong type of operand";
					queue.push(Lex(to_string(res), POL_BOOL));
				} else throw "Different types of operands";

			// operator :=
			} else if (curlex.get_name() == ":=") {
				if ((a.get_type() == b.get_type()) && (a.get_var())) {
					if (a.get_type() == POL_INT) list_v[search_var(a.get_name())].set_value(Lex_to_int(b));
					else if (a.get_type() == POL_REAL) list_v[search_var(a.get_name())].set_value(Lex_to_double(b));
					else if (a.get_type() == POL_BOOL) list_v[search_var(a.get_name())].set_value(Lex_to_bool(b));
					else if (a.get_type() == POL_CHAR) list_v[search_var(a.get_name())].set_value(Lex_to_char(b));
					else throw "Wrong type of operand";
				} else throw "Different types of operands";

			// operator not
			} else if (curlex.get_name() == "not") {
				if (b.get_type() == POL_BOOL) 
					queue.push(Lex(to_string(!Lex_to_bool(b)), POL_BOOL));
				else throw "Wrong type of operand";
			} else throw "Unknown operator";

		// jumps
		} else if (curlex.get_type() == POL_JUMP) {
			if (curlex.get_name() == "!F") {
				Lex tmp = queue.top();
				if (tmp.get_type() != POL_BOOL) throw "Wrong type";
				if (!(Lex_to_bool(tmp))) 
					i = curlex.get_jump() - 1;
			} else if (curlex.get_name() == "!") {
				i = curlex.get_jump() - 1;
			} 

		// input and output
		} else if (curlex.get_type() == POL_IO) {
			if ((curlex.get_name() == ">>") || (curlex.get_name() == "<<")) queue.push(curlex);
			else if ((curlex.get_name() == "write") || (curlex.get_name() == "writeln")) {
				stack<Lex> tmp_queue;
				for (; queue.top().get_name() != ">>"; queue.pop()) tmp_queue.push(queue.top());
				queue.pop();
				for (; !tmp_queue.empty(); tmp_queue.pop()) Lex_out(tmp_queue.top());
				if (curlex.get_name() == "writeln") cout << endl;
			} else if ((curlex.get_name() == "read") || (curlex.get_name() == "readln")) {
				stack<Lex> tmp_queue;
				for (; queue.top().get_name() != "<<"; queue.pop()) tmp_queue.push(queue.top());
				queue.pop();
				for (; !tmp_queue.empty(); tmp_queue.pop()) Lex_in(tmp_queue.top());
				if (curlex.get_name() == "readln") cout << endl;
			}

		// ;
		} else if (curlex.get_type() == POL_SPEC) {
			while (!queue.empty()) queue.pop();
		}
	}
}

int Poliz::Lex_to_int(Lex & L) const {
	int a;
	if (L.get_var())
		a = list_v[search_var(L.get_name())];
	else
		sscanf(L.get_name().c_str(), "%d", &a);
	return a;
}

double Poliz::Lex_to_double(Lex & L) const {
	double a;
	if (L.get_var())
		a = list_v[search_var(L.get_name())];
	else
		sscanf(L.get_name().c_str(), "%lf", &a);
	return a;
}

bool Poliz::Lex_to_bool(Lex & L) const {
	bool a;
	if (L.get_var())
		a = list_v[search_var(L.get_name())];
	else
		a = L.get_name() == "true" ? true : L. get_name() == "1" ? true : false;
	return a;
}

char Poliz::Lex_to_char(Lex & L) const {
	char a;
	if (L.get_var())
		a = list_v[search_var(L.get_name())];
	else
		a = L.get_name()[0];
	return a;
}

void Poliz::Lex_in(Lex & L) {
	if (L.get_var()) {
		if (L.get_type() == POL_INT) {
			int k;
			cin >> k;
			list_v[search_var(L.get_name())].set_value(k);
		} else if (L.get_type() == POL_REAL) {
			double k;
			cin >> k;
			list_v[search_var(L.get_name())].set_value(k);
		} else if (L.get_type() == POL_BOOL) {
			bool k;
			cin >> k;
			list_v[search_var(L.get_name())].set_value(k);
		} else if (L.get_type() == POL_CHAR) {
			char k;
			cin >> k;
			list_v[search_var(L.get_name())].set_value(k);
		}
	} else {
		throw L.get_name() + " isn't variable";
	}
	return;
}

void Poliz::Lex_out(Lex & L) const {
	if (L.get_var()) {
		if (L.get_type() == POL_INT) cout << (int)list_v[search_var(L.get_name())] << flush;
		else if (L.get_type() == POL_REAL) cout << (double)list_v[search_var(L.get_name())] << flush;
		else if (L.get_type() == POL_BOOL) {
			if ((bool)list_v[search_var(L.get_name())]) cout << "true" << flush;
			else cout << "false" << flush;
		}
		else if (L.get_type() == POL_CHAR) cout << (char)list_v[search_var(L.get_name())] << flush;
	} else {
		if (L.get_type() != POL_BOOL) 
			cout << L.get_name() << flush;
		else {
			if (L.get_name() == "1") cout << "true" << flush;
			else cout << "false" << flush;
		}
	}
	return;
}

Poliz::Poliz(const char *filename): psr(filename), list_l(psr.get_list_l()), list_v(psr.get_list_v()) {
	cur_lex = list_l.begin();	
	while (cur_lex->get_name() != "begin") cur_lex = getlex();
	pol_block();
	execute();
	// cout << endl;
	// int i = 0;
	// for (auto T = poliz.begin(); T != poliz.end(); ++T) {
	// 	if (i % 10 == 0) cout << endl;
	// 	cout.setf(ios::left);
	// 	cout << " ";
	// 	cout.width(4);
	// 	cout << i++;
	// 	cout.width(7);
	// 	cout << T->get_name();
	// 	cout.width(3);
	// 	if (T->get_type() == POL_JUMP) cout << T->get_jump();
	// 	else cout << "   ";
	// }
	// cout << endl << endl;
	// for (auto T = poliz.begin(); T != poliz.end(); ++T) {
	// 	cout << *T << endl;
	// }
	// cout  << endl << endl << "============== Poliz is done ==============" << endl << endl;

}