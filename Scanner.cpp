#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <iterator>
#include "Scanner.h"
using namespace std;

const string show(const lex_t & type) {
	switch(type) {
		case LEX_SPEC: return "LEX_SPEC";
		case LEX_WORD: return "LEX_WORD";
		case LEX_NAME: return "LEX_NAME";
		case LEX_NUM: return "LEX_NUM";
		case LEX_STR: return "LEX_STR";
		case POL_JUMP: return "POL_JUMP";
		case POL_OP: return "POL_OP";
		case POL_IO: return "POL_IO";
		case POL_INT: return "POL_INT";
		case POL_REAL: return "POL_REAL";
		case POL_BOOL: return "POL_BOOL";
		case POL_CHAR: return "POL_CHAR";
		case POL_STR: return "POL_STR";
		case POL_SPEC: return "POL_SPEC";
		default: return "";
	}
}

//=================================================================
//=                              Lex                              =
//=================================================================

Lex::Lex (): type(LEX_SPEC), name(""), line(0), column(0) {}

Lex::Lex (const string nm, lex_t tp, int ln, int cl, int pr, bool v): type(tp), name(nm), line(ln), column(cl), priority(pr), var(v) {}

Lex Lex::uncon_jump() {
	Lex tmp("!", POL_JUMP, 0, 0);
	return tmp;
}

Lex Lex::false_jump() {
	Lex tmp("!F", POL_JUMP, 0, 0);
	return tmp;
}

string Lex::get_name() const {
	return name;
}

void Lex::set_name(const string nm) {
	name = nm;
}

lex_t Lex::get_type() const {
	return type;
}

void Lex::set_type(lex_t tp) {
	type = tp;
}

int Lex::get_line() const {
	return line;
}

int Lex::get_column() const {
	return column;
}

int Lex::get_priority() const {
	return priority;
}

void Lex::set_priority(int pr) {
	priority = pr;
}

int Lex::get_jump() const {
	return jump;
}

void Lex::set_jump(int pj) {
	if (type == POL_JUMP) jump = pj;
	else throw "Error of jump initialization";
}

bool Lex::get_var() {
	return var;
}

void Lex::set_var(bool v) {
	var = v;
}

ostream & operator<< (ostream & os, const Lex L) {
	cout << "Name: " << L.name << endl;
	cout << "Type: " << show(L.type) << endl;
	cout << "Line: " << L.line << endl;
	cout << "Column: " << L.column << endl;
	if (L.type == POL_OP) cout << "Priority: " << L.priority << endl;
	if (L.type == POL_JUMP) cout << "Jump on " << L.jump << endl;
	cout << "Var: " << L.var << endl;
	return os;
}

//=================================================================
//=                            Scanner                            =
//=================================================================

void Scanner::Scanner::low(string & s) {
	for (unsigned int i = 0; i < s.length(); ++i) {
		s[i] = tolower(s[i]);
	}
}

bool Scanner::search(const vector<string> & v, const string & str) {
	for (auto T = v.begin(); T != v.end(); ++T) {
		if (*T == str) return true;
	}
	return false;
}

bool Scanner::check_LEX_SPEC(string str) {
	static vector<string> _LEX_SPEC = {"+", "-", "*", "/", ".", ",", ":", ";", "=", 
	"<>", "<", "<=", ">", ">=", ":=", "..", "↑", "^", "(", ")", "[", "]"};
	if (search(_LEX_SPEC, str)) return true;
	else return false;
}

bool Scanner::check_LEX_WORD(string str) {
	static vector<string> _LEX_WORD = {"and", "end", "nil", "set", "array", "file", "not", 
	"then", "begin", "for", "of", "to", "case", "function", "or", "type", "const", 
	"until", "div", "if", "procedure", "var", "do", "in", "program", "while", 
	"label", "record", "with", "else", "mod", "repeat", "packed", "downto"};
	if (search(_LEX_WORD, str)) return true;
	else return false;
}

bool Scanner::check_LEX_NUM(string str) {
	if ((str[0] > '9') || (str[0] < '0')) return false;
	bool flag1 = true, flag2 = true;
	int len = str.length();
	for (int i = 0; i < len; ++i) {
		if (str[i] == '.') {
			//check that . before e
			if (!flag2) 
				return false;
			if (flag1) 
				flag1 = false;
			else
				return false;
		} else if (str[i] == 'e')  {
			if (flag2)	
				flag2 = false;
			else
				return false;
		} else if ((str[i] > '9') || (str[i] < '0'))
			return false;
	}
	return true;
}

bool Scanner::check_NUM(string str) {
	int len = str.length();
	for (int i = 0; i < len; ++i) {
		if ((str[i] > '9') || (str[i] < '0'))
			return false;
	}
	return true;
}

bool Scanner::check_LEX_STR(string str) {
	if ((str[0] == '\'') && (str[str.length() - 1] == '\''))
		return true;
	return false;
}

bool Scanner::checkseparators(char curchar) {
	static string separators = " \n\t";
	if (separators.find(curchar) == string::npos) return false;
	else return true;
}
bool Scanner::checkspecials(char curchar) {
	static string specials = "+-*/.,:;=<>()[]";
	if (specials.find(curchar) == string::npos) return false;
	else return true;
}
bool Scanner::checkbasics(char curchar) {
	static string basics = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_";
	if (basics.find(curchar) == string::npos) return false;
	else return true;
}
void Scanner::addlex(vector<Lex> & list, string & word, int line, int column) {
	if (word.empty()) return;
	lex_t type = LEX_NAME;
	if (check_LEX_STR(word)) type = LEX_STR;
	else low(word);
	if (check_LEX_WORD(word)) type = LEX_WORD;
	if (check_LEX_SPEC(word)) type = LEX_SPEC;
	if (check_LEX_NUM(word)) type = LEX_NUM;
	Lex L(word, type, line, column);
	list.insert(list.end(), L);
}

vector<Lex> Scanner::get_list() const {
	return list;	
}

Scanner::Scanner(const char *filename) {
	ifstream file(filename);
	if (!file.is_open()) {
		cerr << "File isn't opened" << endl;
		throw;
	}
	char curchar, prevchar = ' ';
	int count_of_lines = 1, count_of_columns = 1, line_of_cur_lex = 1, column_of_cur_lex = 1;
	string word;
	word.clear();
	bool flag = true, com = true;
	for (; file.good(); prevchar = com?curchar:prevchar, count_of_columns++) {
		curchar = file.get();
		if (flag) {
			//analyze type of character
			if (checkbasics(curchar)) {
				if (!checkbasics(prevchar) && !((prevchar == '.') && (curchar <= '9') && (curchar >= '0'))) {
					addlex(list, word, line_of_cur_lex, column_of_cur_lex);
					line_of_cur_lex = count_of_lines;
					column_of_cur_lex = count_of_columns;
					word.clear();
				}
				word.push_back(curchar);
			} else if (checkspecials(curchar)) {
				if ((!checkspecials(prevchar)) || check_LEX_SPEC(word)) {
					if (!(((word == "<") && (curchar == '=')) || ((word == "<") && (curchar == '>')) || 
						((word == ">") && (curchar == '=')) || ((word == ":") && (curchar == '=')) || 
						((curchar == '.') && check_NUM(word))))  {
						addlex(list, word, line_of_cur_lex, column_of_cur_lex);
						line_of_cur_lex = count_of_lines;
						column_of_cur_lex = count_of_columns;
						word.clear();
					}
				}
				word.push_back(curchar);
			} else if (checkseparators(curchar)) {
				if (!checkseparators(prevchar)) {
					addlex(list, word, line_of_cur_lex, column_of_cur_lex);
					line_of_cur_lex = count_of_lines;
					column_of_cur_lex = count_of_columns;
					word.clear();
				}
			} else if (curchar == '\'') {
				addlex(list, word, line_of_cur_lex, column_of_cur_lex);
				line_of_cur_lex = count_of_lines;
				column_of_cur_lex = count_of_columns;
				word.clear();
				word.push_back(curchar);
				flag = false;
	
			} else if (curchar == '{') {
				flag = false;
				com = false;
			} else {
				if (curchar != -1) {
					cerr << "Line: " << count_of_lines << endl;
					cerr << "Column: " << count_of_columns << endl;
					throw curchar;
				}
			}
		} else {
			//all characters add into word type independently
			if (com) word.push_back(curchar);
			if ((curchar == '\'') && com) {
				if (!((file.good()) && ('\'' == file.get()))) {
					addlex(list, word, line_of_cur_lex, column_of_cur_lex);
					line_of_cur_lex = count_of_lines;
					column_of_cur_lex = count_of_columns;
					word.clear();
					file.unget();
					flag = true;
				}
			} else if (curchar == '}') {
				flag = true;
				com = true;
			}
		}
		//adjust number of line and number of column	
		if (curchar == '\n') {
			++count_of_lines;
			count_of_columns = 0;
		}
	}
	if (!word.empty()) {
		addlex(list, word, line_of_cur_lex, column_of_cur_lex);
		word.clear();
	}
	
	// // out of lexem list
	// for (auto T = list.begin(); T != list.end(); ++T) {
	// 	cout << *T << endl;
	// }
	// cout  << "============== LA is done ==============" << endl << endl;
}