#include <iostream>
#include <vector>
#include <iterator>
#include "Scanner.h"
#include "Parser.h"
using namespace std;

const string show(const var_t & type) {
	switch(type) {
		case VAR_INT: return "VAR_INT";
		case VAR_BOOL: return "VAR_BOOL";
		case VAR_REAL: return "VAR_REAL";
		case VAR_CHAR: return "VAR_CHAR";
		default: return "";
	}
}

//=================================================================
//=                              Var                              =
//=================================================================

string Var::get_name() const {
	return name;
}

var_t Var::get_type() const {
	return type;
}

void Var::set_value(int val) {
	if (Const) throw *this;
	else val_int = val;
}

void Var::set_value(bool val) {
	if (Const) throw *this;
	else val_bool = val;
}

void Var::set_value(double val) {
	if (Const) throw *this;
	else val_real = val;
}

void Var::set_value(char val) {
	if (Const) throw *this;
	else val_char = val;
}

Var::operator int() const {
	return val_int;
}

Var::operator bool() const {
	return val_bool;
}

Var::operator double() const {
	return val_real;
}

Var::operator char() const {
	return val_char;
}

Var::Var (bool bl, Lex & lex_name, Lex & lex_type, vector<Var> list): Const(bl), name(lex_name.get_name()) {

	// check on original
	for (auto T = list.begin(); T != list.end(); ++T) {
		if (T->get_name() == name) throw lex_name;
	}

	// check on const or variable
	if (bl) {
		if ((lex_type.get_type() == LEX_STR) && (lex_type.get_name().length() == 3)) {
			val_char = lex_type.get_name()[1];
			type = VAR_CHAR;
		} else if ((lex_type.get_name() == "true") || (lex_type.get_name() == "false")) {
			if (lex_type.get_name() == "true") 
				val_bool = true;
			else 
				val_bool = false;
			type = VAR_BOOL;
		} else if (Scanner::check_NUM(lex_type.get_name())) {
			string tmp = lex_type.get_name();
			val_int = 0;
			for (unsigned int i = 0; i < tmp.length(); ++i) {
				val_int = val_int * 10 + tmp[i] - '0';
     		} 
			type = VAR_INT;
		} else if (Scanner::check_LEX_NUM(lex_type.get_name())) {
			sscanf(lex_type.get_name().c_str(), "%lf", &val_real);
			type = VAR_REAL;
		} else {
			cerr << "Unknown type of constant" << endl;
			throw lex_type;
		}
	} else {
		if (lex_type.get_name() == "integer")
			type = VAR_INT;
		else if (lex_type.get_name() == "boolean")
			type = VAR_BOOL;
		else if (lex_type.get_name() == "real")
			type = VAR_REAL;
		else if (lex_type.get_name() == "char")
			type = VAR_CHAR;
		else {
			cerr << "Unknown type of variable" << endl;
			throw lex_type;
		}
	}
}

ostream & operator<< (ostream & os, const Var V) {
	if (V.Const) cout << "Constant" << endl;
	else cout << "Variable" << endl;
	cout << "Name: " << V.name << endl;
	cout << "Type: " << show(V.type) << endl;
	cout << "Value: "; 
	switch(V.type) {
		case VAR_INT: cout << V.val_int << endl; break;
		case VAR_BOOL: cout << V.val_bool << endl; break;
		case VAR_REAL: cout << V.val_real << endl; break;
		case VAR_CHAR: cout << V.val_char << endl; break;
	}
	return os;
}

//=================================================================
//=                            Parser                             =
//=================================================================

vector<Lex>::iterator Parser::getlex() {
	if (++cur_lex != list_l.end())
		return cur_lex;
	return list_l.end();
}

void Parser::P() {

	// block program
	if (cur_lex->get_name() == "program") {
		cur_lex = getlex();
		I();
		if (cur_lex->get_name() == ";") cur_lex = getlex();
		else error();
	}

	// block const
	if (cur_lex->get_name() == "const") C1();

	// block var
	if (cur_lex->get_name() == "var") D1();

	// block begin-end
	B();

	// end of program
	if (cur_lex->get_name() == ".") cur_lex = getlex();
	else error();
	return;
}
void Parser::C1() {
	if (cur_lex->get_name() == "const") cur_lex = getlex();
	else error();
	C();
	while (cur_lex->get_name() == ";") {
		cur_lex = getlex();
		if (cur_lex->get_name() != "var") C();
		else return;
	}
	error();
}
void Parser::C() {
	Lex tmp = *cur_lex;
	I();
	if (cur_lex->get_name() == "=") cur_lex = getlex();
	else error();
	if ((cur_lex->get_type() == LEX_NUM) || (cur_lex->get_type() == LEX_STR)
		|| (cur_lex->get_name() == "true") || (cur_lex->get_name() == "false")) {
		Var var(true, tmp, *cur_lex, list_v);
		list_v.insert(list_v.end(), var);
		cur_lex = getlex();
	}
	else error(); 
}
void Parser::D1() {
	if (cur_lex->get_name() == "var") cur_lex = getlex();
	else error();
	D();
	while (cur_lex->get_name() == ";") {
		cur_lex = getlex();
		if (cur_lex->get_name() != "begin") D();
		else return;	
	}
	error();
}
void Parser::D() {
	vector<Lex> tmp_list;
	tmp_list.insert(tmp_list.end(), *cur_lex);
	I();
	while (cur_lex->get_name() == ",") {
		cur_lex = getlex();
		tmp_list.insert(tmp_list.end(), *cur_lex);
		I();
	}
	if (cur_lex->get_name() == ":") cur_lex = getlex();
	else error();
	if ((cur_lex->get_name() == "integer") || (cur_lex->get_name() == "boolean") 
		|| (cur_lex->get_name() == "real") || (cur_lex->get_name() == "char")) {
		for (auto T = tmp_list.begin(); T != tmp_list.end(); ++T) {
			Var var(false, *T, *cur_lex, list_v);
			list_v.insert(list_v.end(), var);
		}
		cur_lex = getlex();
	}
	else error();
}
void Parser::B() {
	if (cur_lex->get_name() == "begin") cur_lex = getlex();
	else error();
	S();
	while (cur_lex->get_name() == ";") {
		cur_lex = getlex();
		if (cur_lex->get_name() != "end") S();
		else break;
	}

	if (cur_lex->get_name() == "end") cur_lex = getlex();
	else error();
}
void Parser::S() {

	// block if
	if (cur_lex->get_name() == "if") {
		cur_lex = getlex();
		E();
		if (cur_lex->get_name() == "then") cur_lex = getlex();
		else error();
		if (cur_lex->get_name() == "begin") B();
		else S();
		if (cur_lex->get_name() == "else") {
			cur_lex = getlex();
			if (cur_lex->get_name() == "begin") B();
			else S();
		} else return;

	// block while
	} else if (cur_lex->get_name() == "while") {
		cur_lex = getlex();
		E();
		if (cur_lex->get_name() == "do") cur_lex = getlex();
		else error();
		S();

	// block for
	} else if (cur_lex->get_name() == "for") {
		cur_lex = getlex();
		I();
		if (cur_lex->get_name() == ":=") cur_lex = getlex();
		else error();
		if ((cur_lex->get_type() == LEX_STR) || (cur_lex->get_type() == LEX_NUM)) cur_lex = getlex();
		else E1();
		if ((cur_lex->get_name() == "to") || (cur_lex->get_name() == "downto")) cur_lex = getlex();
		else error();
		if ((cur_lex->get_type() == LEX_STR) || (cur_lex->get_type() == LEX_NUM)) cur_lex = getlex();
		else E1();
		if (cur_lex->get_name() == "do") cur_lex = getlex();
		else error();
		S();
		
	// begin-end
	} else if (cur_lex->get_name() == "begin") {
		B();

	// block of read() or readln()
	} else if ((cur_lex->get_name() == "read") || (cur_lex->get_name() == "readln")) {
		cur_lex = getlex();
		if (cur_lex->get_name() == "(") cur_lex = getlex();
		else error();
		if (cur_lex->get_name() != ")") {
			E();
			while (cur_lex->get_name() == ",") {
				cur_lex = getlex();
				E();
			}
		}
		if (cur_lex->get_name() == ")") cur_lex = getlex();
		else error();

	// block of write() or writeln()
	} else if ((cur_lex->get_name() == "write") || (cur_lex->get_name() == "writeln")) {
		cur_lex = getlex();
		if (cur_lex->get_name() == "(") cur_lex = getlex();
		else error();
		if (cur_lex->get_name() != ")") {
			E();
			while (cur_lex->get_name() == ",") {
				cur_lex = getlex();
				E();
			}
		}
		if (cur_lex->get_name() == ")") cur_lex = getlex();
		else error();

	// block of assignment
	} else {
		I();
		if (cur_lex->get_name() == ":=") cur_lex = getlex();
		else error();
		E();
	}
	return;
}

void Parser::E() {
	E1();
	if ((cur_lex->get_name() == "<") || (cur_lex->get_name() == ">") || (cur_lex->get_name() == "=") || 
		(cur_lex->get_name() == "<=") || (cur_lex->get_name() == ">=") || (cur_lex->get_name() == "<>")) cur_lex = getlex();
	else return;
	E1();
	return;
}

void Parser::E1() {
	T();
	while ((cur_lex->get_name() == "+") || (cur_lex->get_name() == "-") || (cur_lex->get_name() == "or")) {
		cur_lex = getlex();
		T();
	}
	return;
}

void Parser::T() {
	F();
	while ((cur_lex->get_name() == "*") || (cur_lex->get_name() == "/") || (cur_lex->get_name() == "and") || 
		(cur_lex->get_name() == "div") || (cur_lex->get_name() == "mod")) {
		cur_lex = getlex();
		F();
	}
	return;
}

void Parser::F() {
	if (cur_lex->get_name() == "(") {
		cur_lex = getlex();
		E();
		if (cur_lex->get_name() == ")") cur_lex = getlex();
		else error();
	} else if (cur_lex->get_name() == "not") {
		cur_lex = getlex();
		F();
	} else if (cur_lex->get_type() == LEX_NUM) {
		cur_lex = getlex();
	} else if (cur_lex->get_type() == LEX_STR) {
		cur_lex = getlex();
	} else if ((cur_lex->get_name() == "true") || (cur_lex->get_name() == "false")) {
		cur_lex = getlex();
	} else {
		I();
	}
	return;
}

void Parser::I() {
	if (cur_lex->get_type() == LEX_NAME) cur_lex = getlex();
	else error();
}

void Parser::error() {
	throw *cur_lex;
}

vector<Lex> Parser::get_list_l() {
	return list_l;
}

vector<Var> Parser::get_list_v() {
	return list_v;
}

Parser::Parser (const char *filename): scr(filename), list_l(scr.get_list()) {
	cur_lex = list_l.begin();
	P();
	if (cur_lex != list_l.end()) error();

	// // out of variable and constant list
	// for (auto T = list_v.begin(); T != list_v.end(); ++T) {
	// 	cout << *T << endl;
	// }
	// cout  << "============== SA is done ==============" << endl << endl;
}