#pragma once
#include <iostream>
#include <vector>
#include <stack>
#include "Scanner.h"
#include "Parser.h"

class Poliz {
	// data
	Parser psr;
	std::vector<Lex> list_l;
	std::vector<Var> list_v;
	std::vector<Lex> poliz;
	std::vector<Lex>::iterator cur_lex;
	std::stack<Lex> queue;
	// methods
	std::vector<Lex>::iterator getlex();
	bool check_operator(std::string str);
	void adjust_operand_lex(Lex & lex);
	void adjust_operator_lex(Lex & lex);
	void pol_block();
	void pol_s();
	void pol_expr();
	int search_var(std::string name) const;
	void execute();
	int Lex_to_int(Lex & L) const;
	double Lex_to_double(Lex & L) const;
	bool Lex_to_bool(Lex & L) const;
	char Lex_to_char(Lex & L) const;
	void Lex_in(Lex & L);
	void Lex_out(Lex & L) const;
public:
	Poliz(const char *filename);
};