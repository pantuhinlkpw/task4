#pragma once
#include <iostream>
#include <vector>
#include <iterator>

typedef enum {LEX_SPEC, LEX_WORD, LEX_NAME, LEX_NUM, LEX_STR, 
POL_JUMP, POL_OP, POL_IO, POL_CHAR, POL_INT, POL_REAL, POL_BOOL, POL_STR, POL_SPEC} lex_t;
const std::string show(const lex_t & type);

class Lex {
	lex_t type;
	std::string name;
	int line;
	int column;
	int priority;
	int jump;
	bool var;
public:
	Lex ();
	Lex (const std::string nm, lex_t tp, int ln = 0, int cl = 0, int pr = 0, bool v = false);
	static Lex uncon_jump();
	static Lex false_jump();
	std::string get_name() const;
	void set_name(const std::string nm);
	lex_t get_type() const;
	void set_type(lex_t tp);
	int get_line() const;
	int get_column() const;
	int get_priority() const;
	void set_priority(int pr);
	int get_jump() const;
	void set_jump(int pj);
	bool get_var();
	void set_var(bool v);
	friend std::ostream & operator<< (std::ostream & os, const Lex L);
};

class Scanner {
	std::vector<Lex> list;
	void low(std::string & s);
	bool check_LEX_SPEC(std::string str);
	bool check_LEX_WORD(std::string str);
	bool check_LEX_STR(std::string str);
	bool checkseparators(char curchar);
	bool checkspecials(char curchar);
	bool checkbasics(char curchar);
	void addlex(std::vector<Lex> & list, std::string & word, int line, int column); 
public:
	static bool search(const std::vector<std::string> & v, const std::string & str);
	static bool check_LEX_NUM(std::string str);
	static bool check_NUM(std::string str);
	std::vector<Lex> get_list() const;
	explicit Scanner(const char *filename);
};