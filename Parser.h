#pragma once
#include <iostream>
#include <vector>
#include <iterator>
#include "Scanner.h"

typedef enum {VAR_INT, VAR_REAL, VAR_BOOL, VAR_CHAR} var_t;
const std::string show(const var_t & type);

class Var {
	bool Const;
	std::string name;
	var_t type;
	int val_int = 0;
	bool val_bool = false;
	double val_real = 0;
	char val_char = '\0';
public:
	std::string get_name() const;
	var_t get_type() const;
	void set_value(int val);
	void set_value(bool val);
	void set_value(double val);
	void set_value(char val);
	operator int() const;
	operator bool() const;
	operator double() const;
	operator char() const;
	Var (bool bl, Lex & lex_name, Lex & lex_type, std::vector<Var> list);
	friend std::ostream & operator<< (std::ostream & os, const Var V);
};

class Parser {
	// data
	Scanner scr;
	std::vector<Lex> list_l;
	std::vector<Lex>::iterator cur_lex;
	std::vector<Var> list_v;
	// methods
	std::vector<Lex>::iterator getlex();
	void P();
	void C1();
	void C();
	void D1();
	void D();
	void B();
	void S();
	void E();
	void E1();
	void T();
	void F();
	void I();
	void error();
public:
	std::vector<Lex> get_list_l();
	std::vector<Var> get_list_v();
	Parser (const char *filename);
};
