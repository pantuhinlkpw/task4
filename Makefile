PROG = Prog
MOD1 = Scanner
MOD2 = Parser
MOD3= Poliz
FLAGS = -g -Wall -std=c++17
IN = input.pas

all: main.o $(MOD1).o $(MOD2).o $(MOD3).o 
	g++ $(FLAGS) $^ -o $(PROG)
%.o: %.cpp
	g++ -c $(FLAGS) $^ -o $@

run:
	./$(PROG) $(IN)
clean:
	rm -f *.o $(PROG)